package org.linxiangyu.android.Z.phone;

import java.util.List;

/**
 * Author: linxiangyu
 * Date:   1/11/14
 * Time:   12:34 PM
 */
public interface SMSInterface {
    public boolean sendSMS(String sendToNumber, String message);
    public boolean writeSendSMSToDatabase();
    public boolean registerListenSMS();
    public List<String> getAllSMS();
    public List<String> getSMSByQuery();
}

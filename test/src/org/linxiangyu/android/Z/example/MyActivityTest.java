package org.linxiangyu.android.Z.example;

import android.test.ActivityInstrumentationTestCase2;
import org.linxiangyu.android.Z.Z;
/**
 * This is a simple framework for a test of an Application.  See
 * {@link android.test.ApplicationTestCase ApplicationTestCase} for more information on
 * how to write and extend Application tests.
 * <p/>
 * To run this test, you can type:
 * adb shell am instrument -w \
 * -e class org.linxiangyu.android.Z.example.MyActivityTest \
 * org.linxiangyu.android.Z.example.tests/android.test.InstrumentationTestRunner
 */
public class MyActivityTest extends ActivityInstrumentationTestCase2<MyActivity> {

    public MyActivityTest() {
        super("org.linxiangyu.android.Z.example", MyActivity.class);
    }

    public void testHelloWorld(){
        assertEquals(Z.returnHelloWorld(), "Hello World");
    }

}
